# DOCKER-VERSION 0.11.1
FROM occam/mjqxgzi--ovrhk3tuouwweyltmuwtembrgqwtanq-
ADD . /simulator
RUN apt-get update
RUN echo 'y' | apt-get install autoconf
RUN cd /simulator; mkdir package; wget https://www.multi2sim.org/files/multi2sim-4.2.tar.gz -O package/package.tar; cd package; tar xvf package.tar
RUN cd /simulator; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /simulator; bash /simulator/build.sh'
CMD ['/bin/bash']
